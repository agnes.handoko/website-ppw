from django.db import models
from django.utils import timezone

# Create your models here.
class ScheduleModel(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)
    waktu = models.DateTimeField()
    tempat = models.CharField(max_length=30)