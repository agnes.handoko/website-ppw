from django import forms

class ScheduleForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    nama_kegiatan = forms.CharField(label='Activity name', required=True, max_length=30, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    waktu = forms.DateTimeField(label='Date', required=True, input_formats=['%Y-%m-%dT%H:%M'], widget=forms.DateTimeInput(attrs={'type':'datetime-local','class':'form-control'}))
    tempat = forms.CharField(label='Place', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='Category', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))