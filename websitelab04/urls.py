"""lab04 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from .views import story3, forms, post_message, table_message, delete_table

urlpatterns = [
    url('story3', story3),
    url('forms', forms),
    url('post_message', post_message, name='post_message'),
    url('table', table_message, name='table_message'),
    url('delete', delete_table, name='delete_table'),
    url('', story3)
]
