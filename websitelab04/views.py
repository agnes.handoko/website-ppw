from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import ScheduleModel

response = {'author' : 'Agnes'}

# Create your views here.
def story3(request):
    return render(request,'story3.html')
    
def forms(request):
    html = 'forms.html'
    response['post_message'] = ScheduleForm # mengambil fungsi dari forms.py
    return render(request, html, response)

def post_message(request):
    form = ScheduleForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['nama_kegiatan']
        response['place'] = request.POST['tempat']
        response['category'] = request.POST['kategori']
        response['time'] = request.POST['waktu']
        message = ScheduleModel(nama_kegiatan=response['name'], tempat=response['place'],
                        kategori=response['category'], waktu=response['time'])
        message.save()
        html ='forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/websitelab04/')

def table_message(request):
    message = ScheduleModel.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html, response)

def delete_table(request):
    ScheduleModel.objects.all().delete()
    response['message'] = ScheduleModel.objects.all()
    html = 'table.html'
    return render(request, html, response)